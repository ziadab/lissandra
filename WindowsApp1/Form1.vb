﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load


    End Sub


    Private Sub total_Click(sender As Object, e As EventArgs) Handles total.Click
        'priceTotal.Text = CType(price1.Text, Integer) + CType(price2.Text, Integer) + CType(price3.Text, Integer) + CType(price4.Text, Integer)

        priceTotal.Text = Val(price1.Text) + Val(price2.Text) + Val(price3.Text) + Val(price4.Text)
    End Sub

    Private Sub AnnulerButton_Click(sender As Object, e As EventArgs) Handles AnnulerButton.Click
        price1.Clear()
        price2.Clear()
        price3.Clear()
        price4.Clear()
        priceTotal.Clear()
        InfoArea.Clear()


    End Sub

    Private Sub ExitButton_Click(sender As Object, e As EventArgs) Handles ExitButton.Click
        Dim result As DialogResult
        result = MessageBox.Show("Do you really want to exit ?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result.Yes Then
            Close()
        ElseIf result.No Then

        End If
    End Sub

    Private Sub MaxButton_Click(sender As Object, e As EventArgs) Handles MaxButton.Click
        InfoArea.Text = getMax(Val(price1.Text), Val(price2.Text), Val(price3.Text), Val(price4.Text))
    End Sub

    Private Function getMax(ByVal a1 As Decimal, ByVal a2 As Decimal, ByVal a3 As Decimal, ByVal a4 As Decimal) As Decimal
        Dim max As Integer

        max = (a1 + a2 + Math.Abs(a1 - a2)) / 2
        max = (max + a3 + Math.Abs(max - a3)) / 2
        max = (max + a4 + Math.Abs(max - a4)) / 2

        Return max

    End Function

    Private Function getMin(ByVal a1 As Decimal, ByVal a2 As Decimal, ByVal a3 As Decimal, ByVal a4 As Decimal) As Decimal
        Dim min As Integer

        min = (a1 + a2 - Math.Abs(a1 - a2)) / 2
        min = (min + a3 - Math.Abs(min - a3)) / 2
        min = (min + a4 - Math.Abs(min - a4)) / 2

        Return min

    End Function

    Private Sub MinButton_Click(sender As Object, e As EventArgs) Handles MinButton.Click
        InfoArea.Text = getMin(Val(price1.Text), Val(price2.Text), Val(price3.Text), Val(price4.Text))

    End Sub
End Class
