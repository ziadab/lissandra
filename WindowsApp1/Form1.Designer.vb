﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.price1 = New System.Windows.Forms.TextBox()
        Me.price2 = New System.Windows.Forms.TextBox()
        Me.price3 = New System.Windows.Forms.TextBox()
        Me.price4 = New System.Windows.Forms.TextBox()
        Me.priceTotal = New System.Windows.Forms.TextBox()
        Me.total = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.InfoArea = New System.Windows.Forms.TextBox()
        Me.MinButton = New System.Windows.Forms.Button()
        Me.MaxButton = New System.Windows.Forms.Button()
        Me.AnnulerButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'price1
        '
        Me.price1.Location = New System.Drawing.Point(159, 56)
        Me.price1.Name = "price1"
        Me.price1.Size = New System.Drawing.Size(169, 22)
        Me.price1.TabIndex = 0
        '
        'price2
        '
        Me.price2.Location = New System.Drawing.Point(159, 95)
        Me.price2.Name = "price2"
        Me.price2.Size = New System.Drawing.Size(169, 22)
        Me.price2.TabIndex = 1
        '
        'price3
        '
        Me.price3.Location = New System.Drawing.Point(159, 137)
        Me.price3.Name = "price3"
        Me.price3.Size = New System.Drawing.Size(169, 22)
        Me.price3.TabIndex = 2
        '
        'price4
        '
        Me.price4.Location = New System.Drawing.Point(159, 177)
        Me.price4.Name = "price4"
        Me.price4.Size = New System.Drawing.Size(169, 22)
        Me.price4.TabIndex = 3
        '
        'priceTotal
        '
        Me.priceTotal.Location = New System.Drawing.Point(159, 218)
        Me.priceTotal.Name = "priceTotal"
        Me.priceTotal.ReadOnly = True
        Me.priceTotal.Size = New System.Drawing.Size(169, 22)
        Me.priceTotal.TabIndex = 4
        '
        'total
        '
        Me.total.Location = New System.Drawing.Point(373, 98)
        Me.total.Name = "total"
        Me.total.Size = New System.Drawing.Size(75, 23)
        Me.total.TabIndex = 5
        Me.total.Text = "Total"
        Me.total.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(84, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Prix Article 1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(84, 98)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Prix Article 2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(84, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Prix Article 3"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(84, 180)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Prix Article 4"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(100, 221)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Prix Total"
        '
        'ExitButton
        '
        Me.ExitButton.Location = New System.Drawing.Point(12, 269)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(75, 23)
        Me.ExitButton.TabIndex = 11
        Me.ExitButton.Text = "Exit"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(188, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 28)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Facture"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(156, 254)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(28, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Info"
        '
        'InfoArea
        '
        Me.InfoArea.Location = New System.Drawing.Point(190, 251)
        Me.InfoArea.Name = "InfoArea"
        Me.InfoArea.Size = New System.Drawing.Size(138, 22)
        Me.InfoArea.TabIndex = 14
        '
        'MinButton
        '
        Me.MinButton.Location = New System.Drawing.Point(373, 137)
        Me.MinButton.Name = "MinButton"
        Me.MinButton.Size = New System.Drawing.Size(75, 23)
        Me.MinButton.TabIndex = 15
        Me.MinButton.Text = "Minimum"
        Me.MinButton.UseVisualStyleBackColor = True
        '
        'MaxButton
        '
        Me.MaxButton.Location = New System.Drawing.Point(373, 180)
        Me.MaxButton.Name = "MaxButton"
        Me.MaxButton.Size = New System.Drawing.Size(75, 23)
        Me.MaxButton.TabIndex = 16
        Me.MaxButton.Text = "Maximum"
        Me.MaxButton.UseVisualStyleBackColor = True
        '
        'AnnulerButton
        '
        Me.AnnulerButton.Location = New System.Drawing.Point(373, 221)
        Me.AnnulerButton.Name = "AnnulerButton"
        Me.AnnulerButton.Size = New System.Drawing.Size(75, 23)
        Me.AnnulerButton.TabIndex = 17
        Me.AnnulerButton.Text = "Annuler"
        Me.AnnulerButton.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(476, 304)
        Me.Controls.Add(Me.AnnulerButton)
        Me.Controls.Add(Me.MaxButton)
        Me.Controls.Add(Me.MinButton)
        Me.Controls.Add(Me.InfoArea)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.total)
        Me.Controls.Add(Me.priceTotal)
        Me.Controls.Add(Me.price4)
        Me.Controls.Add(Me.price3)
        Me.Controls.Add(Me.price2)
        Me.Controls.Add(Me.price1)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Name = "Form1"
        Me.Text = "Facture"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents price1 As TextBox
    Friend WithEvents price2 As TextBox
    Friend WithEvents price3 As TextBox
    Friend WithEvents price4 As TextBox
    Friend WithEvents priceTotal As TextBox
    Friend WithEvents total As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents ExitButton As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents InfoArea As TextBox
    Friend WithEvents MinButton As Button
    Friend WithEvents MaxButton As Button
    Friend WithEvents AnnulerButton As Button
End Class
